### ----------------- Importation des Modules ---------------------##

#Importation des modules pour le fonctionnement de l'interface
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
from tkinter import filedialog
import os
import shutil
import webbrowser

#Importation des modules du projet 
from GeoRetrievingData import parsingGeofile
from GeoRetrievingData import convertID
from GeoRetrievingData import importURLUniprot
from GeoRetrievingData import dataUniprotToTxt
from GeoRetrievingData import complementaryData


### ------------------- Tab system creation ------------------------##
root = Tk()
root.title('GEO Retrieving Data')
root.geometry("860x550")
root.minsize(860,550)
root.maxsize(860,550)
root.iconbitmap("logos/logoApp.ico") #définition du logo associé à l'application
root.config(background="white") #couleur de fond de l'application

style = ttk.Style()
style.configure("BW.TLabel", foreground="black", background="white")
n = ttk.Notebook(root); n.pack()


home = ttk.Frame(n, style="BW.TLabel"); home.pack()
analysis = ttk.Frame(n, style= "BW.TLabel"); analysis.pack()
n.add(home, text='Home')
n.add(analysis, text='Analysis')


### ---------------------- Home Tab ------------------------------##

## _______________________ Mise en forme ________________________ ## 

###### Ajout des logos #########


img_ipmc = PhotoImage(file = 'logos/logoIPMC.png')
img_ipmc = img_ipmc.zoom(17) 
img_ipmc = img_ipmc.subsample(20)

ipmc_logo = Label(home, image = img_ipmc, bg= 'white')
ipmc_logo.grid(row=0, column=0)

img_uca = PhotoImage(file = 'logos/logoUCA.png')
img_uca = img_uca.zoom(1) 
img_uca = img_uca.subsample(2)
uca_logo = Label(home, image = img_uca, bg= "white")
uca_logo.grid(row=0, column=1)

img_polytech = PhotoImage(file = 'logos/logoPolytech.png')
polytech_logo = Label(home, image = img_polytech, bg= "white")
polytech_logo.grid(row=0, column=2)

Espace = Label(home, text = "      ", bg = "white")
Espace.grid(row= 0, column=3, columnspan = 2, ipady=15)

###### Ajout des images #########
Espace = Label(home, text = "      ", bg = "white")
Espace.grid(row= 1, column=1, ipady=15)

imgComputer = PhotoImage(file="logos/logoORDI.png")
imgComputer = imgComputer.zoom(1) 
imgComputer= imgComputer.subsample(4)
logoComputer = Label(home, image = imgComputer,  bg="white")
logoComputer.grid(row=2, column = 1)

Espace = Label(home, text = "      ", bg = "white")
Espace.grid(row= 1, column=3)

##### Ajout du texte ##########


label_title = Label(home, text="Welcome to the GEO Retrieving Data Application",font=("Helvetica", 15), fg='black', bg= "white")
label_title.grid(row=3, column=1)

label_subtitle = Label(home, text="From GEO data to readable and easily analysable file", font=("Helvetica", 12), fg='black', bg= "white")
label_subtitle.grid(row=4, column=1, ipady = 10)

space = Label(home, text="                      ", bg ="white")
space.grid(row=5, column =1)

info = Label(home, text="Please make sure to be connected to the Internet before using the application.", font = ("Helvetica", 10), fg="dark red", bg ="white")
info.grid(row=6, column =1)
info = Label(home, text="Some functionalities of the application will not be accessible without a connexion.", font = ("Helvetica", 8), fg="black", bg ="white")
info.grid(row=7, column =1)


##_______________________ Fonctionnalités _____________________##


def openFile():
     if (os.path.exists("helpFile.html")):
        webbrowser.open('file://' + os.path.realpath('helpFile.html'),new = 2 )
     else:
        messagebox.showwarning(title = "ERROR", message = 'Error. Please close and reopen the app to access the help file.')

space = Label(home, text="                      ", bg ="white")
space.grid(row=7, column =0)


##Afficher une fenêtre pop-up d'explication 
btnAbout = Button(home, text='Help ?', bg = "#545358", fg= "white", command=openFile)
btnAbout.grid(row=8, column=0)



## ----------------------------  Analysis ---------------------------##


##__________________________ Fonctionnalités ______________________________##


############## Fonctions pour le traitement des fichiers ##############


def browseFile():
    """Récupération du fichier et affichage de la liste de gènes"""
    global filename
    try :
        try:
            folder.destroy()
        except:
            geneListBox.delete(0,END) #Vide la listBox pour faire une 2ème analyse de suite
            uncolorButton(btnFileLoad)            
            filename = filedialog.askopenfilename(filetypes=[("Text files","*.txt")])
            if parsingGeofile.geoprofileTrans(filename) == False :
                os.remove("TransitionalGeoFile.txt") #Suppression du fichier
                messagebox.showwarning(title = "ERROR", message = 'Please load the text file corresponding to the geoprofile data')
            else :
                dicoGene = parsingGeofile.recupIDgene("TransitionalGeoFile.txt")
                os.remove("TransitionalGeoFile.txt") #Suppression du fichier
                for cle in dicoGene:
                    geneListBox.insert(END, cle)
                colorButton(btnFileLoad)
                return filename
        else :
            geneListBox.delete(0,END) #Vide la listBox pour faire une 2ème analyse de suite
            uncolorButton(btnFileLoad)
            filename = filedialog.askopenfilename(filetypes=[("Text files","*.txt")])
            if parsingGeofile.geoprofileTrans(filename) == False :
                os.remove("TransitionalGeoFile.txt") #Suppression du fichier
                messagebox.showwarning(title = "ERROR", message = 'Please load the text file corresponding to the geoprofile data')
            else:
                dicoGene = parsingGeofile.recupIDgene("TransitionalGeoFile.txt")
                os.remove("TransitionalGeoFile.txt") #Suppression du fichier
                for cle in dicoGene:
                    geneListBox.insert(END, cle)
                colorButton(btnFileLoad)
                return filename
    except:
       messagebox.showwarning(title = "ERROR", message = 'No file selected')


def browseOutDir():
    """"Récupération du PATH du répertoire de sortie"""
    global out_path
    out_path = filedialog.askdirectory()
    print(out_path)
    global folder
    try :
        folder.destroy()
    except :
        print("first directory selected")
        folder = Label(analysis, text = out_path, font=("Helvetica", 9),  bg='white', fg="grey")
        folder.grid(row= 5, column= 3)
    else:
        folder = Label(analysis, text = out_path, font=("Helvetica", 9),  bg='white', fg="grey")
        folder.grid(row= 5, column= 3)
    return out_path

    
def retrieveGeneSelected():
    """Récupération des gènes sélectionnés (fonction associée au bouton 'Valider')"""
    values = [geneListBox.get(idx) for idx in geneListBox.curselection()]
    print(values)
    return values

def selectAll():
    """Selectionne tous les gènes de la liste"""
    geneListBox.select_set(0,END)


def colorButton(bouton):
    """Change la couleur du bouton"""
    bouton.configure(bg= "#98C9A9")

def uncolorButton(bouton):
    """Remet la couleur initiale du bouton"""
    bouton.configure(bg ="#E9EAE9")
    
def retrieveGEOL():
    """Récupération du fichier traité GEO long"""
    try :
        parsingGeofile.geoprofile_long(filename)
    except :
        messagebox.showwarning(title = "ERROR", message = 'Please load the file before running the analysis.')
    else : 
        values = retrieveGeneSelected()
        if len(values) == 0 :
            if (os.path.exists("GEO_FullReorganisedFile.txt")):
                os.remove("GEO_FullReorganisedFile.txt")
            messagebox.showwarning(title = "ERROR", message = 'Please select at list a gene name before running the analysis')
        else :
            try :
                print(out_path)
            except :
                if (os.path.exists("GEO_FullReorganisedFile.txt")):
                    os.remove("GEO_FullReorganisedFile.txt")
                messagebox.showwarning(title = "ERROR", message = 'Please select an output directory.')
            else :
               if len(out_path)> 3:
                    if not (os.path.exists(out_path +"/GEO_FullReorganisedFile.txt")):
                        shutil.move("GEO_FullReorganisedFile.txt", out_path)
                    os.chdir(out_path)
                    if (os.path.exists("GEO_TotalReorganised.txt")):
                         os.remove("GEO_FullReorganisedFile.txt")
                         messagebox.showwarning(title = "ERROR", message = 'The file is already existing. Please delete it or change the output directory before running the analysis.')
                    else :
                         parsingGeofile.geoprofile_long_geneSelection(out_path +"/GEO_FullReorganisedFile.txt", values)
                         os.remove("GEO_FullReorganisedFile.txt")
                         parsingGeofile.clean_GEO_TotalReorganised()
                         os.remove("GEO_TotalReorganisedNotCleaned.txt")
                         messagebox.showinfo(title = "Done", message = 'Analysis done. You can find the file in the folder dedicated.')
               else :
                    if (os.path.exists("GEO_FullReorganisedFile.txt")):
                         os.remove("GEO_FullReorganisedFile.txt")
                    messagebox.showwarning(title = "ERROR", message = 'Please select an output directory.') 


def retrieveGEOC():
    """Récupération du fichier traité GEO court"""
    try :
        parsingGeofile.geoprofileTrans(filename)
    except :
        messagebox.showwarning(title = "ERROR", message = 'Please load the file before running the analysis.')
    else :
        values = retrieveGeneSelected()
        if len(values) == 0 :
            if (os.path.exists("TransitionalGeoFile.txt")):
                os.remove("TransitionalGeoFile.txt")
            messagebox.showwarning(title = "ERROR", message = 'Please select at list a gene name before running the analysis')
        else :
            try:
                print(out_path)
            except :
                if (os.path.exists("TransitionalGeoFile.txt")):
                    os.remove("TransitionalGeoFile.txt")
                messagebox.showwarning(title = "ERROR", message = 'Please select an output directory.')
            else:
               if len(out_path)> 3:
                    if not (os.path.exists(out_path + "/TransitionalGeoFile.txt")):
                        shutil.move("TransitionalGeoFile.txt", out_path)
                    os.chdir(out_path)
                    if (os.path.exists("GEO_MinimalReorganised.txt")):
                         os.remove("TransitionalGeoFile.txt")
                         os.remove("GEO_MinimalReorganisedTrans.txt")
                         messagebox.showwarning(title = "ERROR", message = 'The file is already existing. Please delete it or change the output directory before running the analysis.')
                    else:
                         parsingGeofile.geoprofileTrans_geneSelection("TransitionalGeoFile.txt", values)
                         parsingGeofile.geoprofile_court("GEO_Trans_GeneSelection.txt")
                         parsingGeofile.clean_GEO_MinimalReorganised()
                         os.remove("TransitionalGeoFile.txt")
                         os.remove("GEO_Trans_GeneSelection.txt")
                         os.remove("GEO_MinimalReorganisedTrans.txt")
                         messagebox.showinfo(title = "Done", message = 'Analysis done. You can find the file in the folder dedicated.')
               else :
                    if (os.path.exists("/TransitionalGeoFile.txt")):
                        os.remove("TransitionalGeoFile.txt")
                    messagebox.showwarning(title = "ERROR", message = 'Please select an output directory.') 

def retrieveUniprotFile():
    """Récupération des fiches Uniprots relatives à un fichier GEO et des condensés de ces fiches"""
    try :
        parsingGeofile.geoprofileTrans(filename)
    except :
          messagebox.showwarning(title = "ERROR", message = 'Please load the file before running the analysis')
    else:
        values = retrieveGeneSelected()
        if len(values) == 0 :
          if (os.path.exists("TransitionalGeoFile.txt")):
               os.remove("TransitionalGeoFile.txt")
          messagebox.showwarning(title = "ERROR", message = 'Please select at list a gene name before running the analysis')
        else :
            parsingGeofile.geoprofileTrans_geneSelection("TransitionalGeoFile.txt", values)
            try :
                print(out_path)
            except :
                if (os.path.exists("TransitionalGeoFile.txt")):
                     os.remove("TransitionalGeoFile.txt")
                if (os.path.exists("GEO_Trans_GeneSelection.txt")):
                     os.remove("GEO_Trans_GeneSelection.txt")
                messagebox.showwarning(title = "ERROR", message = 'Please select an output directory.')
            else :
                if len(out_path) > 3:
                    if not (os.path.exists(out_path + "/TransitionalGeoFile.txt")):
                        shutil.move("TransitionalGeoFile.txt", out_path)
                    if not (os.path.exists(out_path + "/GEO_Trans_GeneSelection.txt")):
                        shutil.move("GEO_Trans_GeneSelection.txt", out_path)
                        
                    os.chdir(out_path)
                    progress = ttk.Progressbar(myFrame, orient = HORIZONTAL, length =100, mode='determinate')
                    progress.grid(row=8, column = 10)
            
                    progress['value'] = 10
                    analysis.update_idletasks()
            
                    dicoGene = parsingGeofile.recupIDgene(out_path + "/GEO_Trans_GeneSelection.txt")
                    print(dicoGene)
                    os.remove("TransitionalGeoFile.txt")
                    os.remove("GEO_Trans_GeneSelection.txt")
            
                    progress['value'] = 20
                    analysis.update_idletasks()

                    try :
                        listeGeneName = convertID.convertID(dicoGene)
                        print(listeGeneName)
                    except :
                        messagebox.showwarning(title = "ERROR", message = 'Please connect your computer to the Internet before running the analysis.')
                        progress.destroy()
                    else :
                        progress['value'] = 30
                        analysis.update_idletasks()
                
                        listeIdUniprot, listeIdGene = convertID.filetoListAN(out_path + "/GNtoUniprotID.txt", listeGeneName, dicoGene)
                        print(listeIdGene)
                        os.remove("GNtoUniprotID.txt")
                        
                        progress['value'] = 50
                        analysis.update_idletasks()

                        i = 45/len(listeIdUniprot)
                
                        for k in range(len(listeIdUniprot)):
                            ficheUniprot = importURLUniprot.URL(listeIdUniprot[k], listeIdGene[k], out_path)
                            dicoDataUniprot = dataUniprotToTxt.searchDataUniprot(listeIdUniprot[k],listeIdGene[k], out_path)
                            try : 
                                cleanFileUniprot = dataUniprotToTxt.writeFileTxt(dicoDataUniprot, out_path)
                            except :
                                errorFile = open("UniprotOverviewError.txt", "a")
                                errorFile.write("Error: impossible to retrieve the data from the Uniprot file {0} concerning the gene whose ID is {1}. \n".format(listeIdUniprot[k], listeIdGene[k]))
                                errorFile.close()
                            else:
                                progress['value'] = progress['value'] + i
                                analysis.update_idletasks()
                    
                        progress['value'] = 100

                        keepUniprot = messagebox.askyesno(title = "Uniprot Files", message = 'Do you want to keep the Uniprot files associated with the gene studied ?')
                        if keepUniprot == 0 :
                            for k in range(len(listeIdUniprot)):
                                os.remove("PDB_Gene-{}_AN-{}.txt".format(listeIdGene[k],listeIdUniprot[k]))
                        progress.grid_forget()
                else :
                    if (os.path.exists("TransitionalGeoFile.txt")):
                         os.remove("TransitionalGeoFile.txt")
                    if (os.path.exists("GEO_Trans_GeneSelection.txt")):
                         os.remove("GEO_Trans_GeneSelection.txt")
                    messagebox.showwarning(title = "ERROR", message = 'Please select an output directory.')
                

def retrieveComplementaryData():
    """Récupération de graphiques et fichier CSV associé aux gènes sélectionnés"""
    try :
        parsingGeofile.geoprofile_long(filename)
    except :
        messagebox.showwarning(title = "ERROR", message = 'Please load the file before running the analysis.')
    else :
        try :
            print(out_path)            
        except :
            if (os.path.exists("GEO_FullReorganisedFile.txt")):
                 os.remove("GEO_FullReorganisedFile.txt")
            messagebox.showwarning(title = "ERROR", message = 'Please select an output directory.')
        else :
            if len(out_path) > 3:
               if not (os.path.exists(out_path + "/GEO_FullReorganisedFile.txt")):
                    shutil.move("GEO_FullReorganisedFile.txt", out_path)
               values = retrieveGeneSelected()
            else :
               if (os.path.exists("GEO_FullReorganisedFile.txt")):
                    os.remove("GEO_FullReorganisedFile.txt")
               messagebox.showwarning(title = "ERROR", message = 'Please select an output directory.')
                
            if len(values) == 0 :
                messagebox.showwarning(title = "ERROR", message = 'Please select at list a gene name before running the analysis.')
            else :
                os.chdir(out_path)
                progress = ttk.Progressbar(myFrame, orient = HORIZONTAL, length =100, mode='determinate')
                progress.grid(row=9, column = 10)
                progress['value'] = 15
                i = 75/len(values)
                graphError = []
                for geneName in values :
                    try : 
                        complementaryData.histo_data(geneName,"GEO_FullReorganisedFile.txt")
                        complementaryData.csv_data(geneName,"GEO_FullReorganisedFile.txt")
                    except :
                        graphError.append(geneName)
                        errorFile = open("Charts-CSV_Error.txt", "a")
                        errorFile.write("Error: impossible to retrieve the complementary data for {}\n".format(geneName))
                        errorFile.close()
                    else :
                        progress['value'] = progress['value'] + i
                os.remove(out_path + "/GEO_FullReorganisedFile.txt")
                if (os.path.exists("Charts-CSV_Error.txt")):
                    for k in range(len(graphError)):
                         if (os.path.exists("Expression{}.pdf".format(graphError[k]))):
                              try : 
                                   os.remove("Expression{}.pdf".format(graphError[k]))
                              except :
                                   PDFerrorFile = open("PDF_ErrorFile.txt", "a")
                                   PDFerrorFile.write("Impossible to delete damaged PDF file corresponding to the gene : {}\n".format(graphError[k]))
                                   PDFerrorFile.close()
                progress['value'] = 100
                messagebox.showinfo(title = "Done", message = 'Analysis done. You can find the file in the folder dedicated.')
                progress.grid_forget()



## _______________________ Mise en forme ________________________ ##

    


browseFileTxt = Label(analysis, text = "Select the file to analyse : ", bg="white", font=("Helvetica", 10))
browseFileTxt.grid(row=0, column=0,columnspan=2, ipady=3)

##Bouton pour charger le fichier 
btnFileLoad = Button(analysis, text='Browse',  command = browseFile)
btnFileLoad.grid(row=0, column=2)

chooseGeneTxt = Label(analysis, text = "Select genes of interest : ", bg="white", font=("Helvetica", 10))
chooseGeneTxt.grid(row=1, column=1, columnspan=2, ipady=10, ipadx = 5)

##Ajouter une scrollbar pour la liste de gènes

height = 50
my_frame = Frame(analysis)

global geneListBox

my_scrollbar = Scrollbar(my_frame, orient=VERTICAL)
geneListBox = Listbox(my_frame, yscrollcommand=my_scrollbar.set, selectmode=MULTIPLE)
my_scrollbar.config(command=geneListBox.yview)

my_frame.grid(row=2, column = 2, columnspan=2)
my_scrollbar.grid(row=2, column = 4, ipady = 80)
geneListBox.grid(row = 2, column = 2,columnspan=2, ipady = 25, ipadx= 10)

##Bouton pour sélectionner tous les gènes
selectAllGene = Button(analysis, text="Select All", command = selectAll)
selectAllGene.grid(row = 3, column = 4)

DestFileTxt = Label(analysis, text = "Select file destination : ", bg="white", font=("Helvetica", 10))
DestFileTxt.grid(row= 5, column=0 ,columnspan=2, ipady=10)

##Bouton Destination File
btnFileOut = Button(analysis, text='Browse', command = browseOutDir)
btnFileOut.grid(row=5, column=2)


ResultFileTxt = Label(analysis, text = "Result files :  ", bg= "white", font=("Helvetica", 10))
ResultFileTxt.grid(row= 6, column=0, columnspan =2, ipady=10)

myFrame = Frame(analysis)
myFrame.grid(row = 7, column = 0, rowspan = 6, columnspan = 6, ipadx=110, ipady=25)

FullGeoTxt = Label(myFrame, text = "GEO file containing all the information :  ", font=("Helvetica", 10))
FullGeoTxt.grid(row= 8, column=1,columnspan=2, ipady=10)

#Bouton Utilisateur pour obtenir le fichier GEO long
btnFile_GeoL = Button(myFrame, text='Download', command = retrieveGEOL)
btnFile_GeoL.grid(row=8, column=3, columnspan = 2)

ShortGeoTxt = Label(myFrame, text = "GEO file containing only essential information :  ", font=("Helvetica", 10))
ShortGeoTxt.grid(row= 9, column=1,columnspan=2, ipady=10)

#Bouton Utilisateur pour obtenir le fichier GEO court
btnFile_GeoC = Button(myFrame, text='Download', command = retrieveGEOC)
btnFile_GeoC.grid(row=9, column=3, columnspan = 2)


Espace = Label(myFrame, text = "      ")
Espace.grid(row= 8, column=5, ipady=15)

UniprotTxt = Label(myFrame, text = "Uniprot files associated :  ", font=("Helvetica", 10))
UniprotTxt.grid(row= 8, column=6, columnspan=2, ipady=10)

#Bouton Utilisateur pour obtenir les fichier Uniprot
btnFile_Uniprot = Button(myFrame, text="Download", command = retrieveUniprotFile)
btnFile_Uniprot.grid(row = 8, column=8, columnspan = 2)

Espace = Label(myFrame, text = "      ")
Espace.grid(row= 9, column=5, ipady=15)

pdfTxt = Label(myFrame, text = "Complementary Charts and Tabs :  ", font=("Helvetica", 10))
pdfTxt.grid(row= 9, column= 6, columnspan=2, ipady=10)

#Bouton Utilisateur pour obtenir les fichier Uniprot
btnPDF = Button(myFrame, text="Download", command = retrieveComplementaryData)
btnPDF.grid(row = 9, column=8, columnspan = 2)

root.mainloop()
