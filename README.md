# GEO Parsing and Retrieving Data

The GEO Parsing and Retrieving Data App is a Python Apps developed using the tkinter package that aims to ease the analysis of raw data from GEO Profile text file and the search of complementary resources concerning the genes of interest.


### Installation
You can install the tool by downloading the zip folder in this page (Download -> zip).

You can also install the tool from source using git : <br><br>
`cd "path of the directory of installation"`<br>
`git clone https://gitlab.com/CelieDs/biologie-integrative.git `

Please make sure to have Python (version > 3) downloaded on your computer.

### Best Practices

The application is launched by clicking on the "Application_GeoRetrievingData.py" file. You can also launch the application by opening the file in a text editor for code (i.e. Python IDLE) and by running its content. 

Before using the tool please make sure to be connected to the Internet. 
Please make sure that the output repository is empty before running a new analysis. 

A more detailed notice can be found on the app by clicking on the "Help ?" button. 
