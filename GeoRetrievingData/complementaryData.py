import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.font_manager import FontProperties
import matplotlib.patches as mpatches
import csv
import random

def pull_data(id_gene, fichier):
    f=open(fichier,'r')
    line=f.readlines()
    liste=[]
    for i in range(len(line)) :
        a=line[i].split("\t")
        if id_gene in a[1]:
            gds=[]
            treat=[]
            value=[]
            j=i
            while a[5][:3] != 'GSM':
                j-=1
                a=line[j].split("\t") 
            nb=0
            for y in a :
                if y[:3] == 'GSM':
                    nb+=1
            c=5
            gds.append(line[j+1].rstrip().split("\t")[3])
            while c < (nb+5) :
                treat.append(line[j+1].rstrip().split("\t")[c])
                value.append(line[i].split("\t")[c])
                c+=1
            liste.append(gds)
            liste.append(treat)
            liste.append(value)
            
    return liste
            


def histo_data(id_gene,fichier):
    
    donnees=pull_data(id_gene,fichier)
    #indice_value=len(donnees)/

    if "///" in id_gene :
        id_gene=id_gene.replace("///","_")
        
    pp = PdfPages('Expression'+id_gene+'.pdf')

    for i in range(0,len(donnees),3):
        liste_int=[]
        for j in donnees[i+2]:
            if j[-1]=="]" :
                j=j[:-3]
            liste_int.append(float(j))

        #Faire les couleurs    
        colors = ('#E179B7', '#2161E1', '#E16733', '#55AEE1', '#518B4D', '#8B6649','#8B0C2A','#8B6F79','#2D8B88')
        couleur=[]

        col=random.choice(colors)
        couleur.append(col)
        for j in range(1,len(donnees[i+1])):
            if donnees[i+1][j]==donnees[i+1][j-1] :
                couleur.append(col)
            else :
                col=random.choice(colors)
                couleur.append(col)

        #Faire la legende
        col_list = [] 
        for j in couleur : 
            if j not in col_list: 
                col_list.append(j)

        title_list=[] 
        for j in donnees[i+1] : 
            if j not in title_list: 
                title_list.append(j)

        legend_list=[]
        for j in range(len(title_list)):
            legend_list.append(mpatches.Patch(color=col_list[j], label=title_list[j]))
        print(legend_list)
               
            
     
        # Make a fake dataset:
        height = liste_int
        bars = donnees[i+1]
        y_pos = np.arange(len(bars))

        figure = plt.figure()
            
        # Create bars
        plt.bar(y_pos, height, color=couleur)
        
        # Create names on the x-axis
        #plt.xticks(y_pos, bars, rotation = 65)
        #plt.xticks([])
        #plt.set_xticklabels([daysofweek[i][0] for i in xval])
        plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False)
        
        plt.tight_layout()

        #plt.figure(figsize=(3, 3))
        gds=str(donnees[i])
        plt.title(gds[2:-2]+" Expression profil of  "+id_gene)
        plt.legend(handles=legend_list, loc='best') 
        #figure.show()
        #plt.savefig(id_gene+'_plot_transcriptomique.png',bbox_inches="tight")
        pp.savefig(figure,bbox_inches="tight")  
        plt.close()
    pp.close()


def csv_data(id_gene,fichier):
    
    donnees=pull_data(id_gene, fichier)
    
    f=open("transcriptomicData.csv","a")
    f.write(id_gene+"\n")
    for i in range(0,len(donnees),3):
        f.write(str(donnees[i])[2:-2]+"\n")
        print(str(donnees[i])[2:-2]+"\n")
        for j in range(len(donnees[i+1])):
            f.write(donnees[i+1][j]+";")
        f.write("\n")
        for j in range(len(donnees[i+2])):
            f.write(donnees[i+2][j]+";")
        f.write("\n")
    f.write("\n")
    f.close()

