#Importation des modules
import urllib.parse
import urllib.request
import os

#Fonctions

def convertID(dicoGene_ID):
    """Fonction utilise un dictionnaire contenant les identifiants des gènes et créer un fichier .txt
    contenant le nom du gène suivi de l'identifiant du gène et de sa fiche Uniprot"""
    
    listGeneName = []
    listGeneID=[]
    
    for cle in dicoGene_ID:
        listGeneName.append(cle)
        listGeneID.append(dicoGene_ID[cle])
    path = os.getcwd()
    file = open("GNtoUniprotID.txt", "w")
    
    for k in range(len(listGeneID)):
        url = 'https://www.uniprot.org/uploadlists/'
        params = {
        'from': 'P_ENTREZGENEID',
        'to': 'ACC',
        'format': 'tab',
        'query': listGeneID[k]
        }
        data = urllib.parse.urlencode(params)
        data = data.encode('utf-8')
        
        try:
            req = urllib.request.Request(url, data)

        except:
            print("Error: please make sur to be connected to the Internet")
            
        else:
            with urllib.request.urlopen(req) as f:
                response = f.read()
                file.write("GN : {} \n".format(listGeneName[k]))
                file.write(response.decode('utf-8'))

    file.close()
    return listGeneName
    
#convertID({'gene1':'21837392','gene2':'51564','gene3':'140654'})

def filetoListAN(file, listGeneName, dicoGeneID):
    """Fonction qui récupère les id des gènes et Uniprot d'un fichier texte et les stocke dans deux listes distinctes"""
    listGN = []
    listAN = []
    f=open(file, "r")
    line = f.readline()
    while line != "":
        if 'GN' in line :
            line = f.readline()

        elif 'From' in line :
            line = f.readline()

        else :
            i=0
            GN=""
            AN=""
            while line[i] != "\t":
                GN=GN+line[i]
                i=i+1
            listGN.append(GN.strip())
            while line[i] != "\n":
                AN = AN+line[i]
                i=i+1
            listAN.append(AN.strip())
            
        line = f.readline()
    f.close()

    ## Edition du fichier d'information à l'utilisateur si la conversion de l'ID d'un gène
    ## en numéro d'accession de la fiche Uniprot n'a pas pu aboutir
    listGNmin = []
    for GeneID in listGN :
        if GeneID not in listGNmin :
            listGNmin.append(GeneID)
    if (len(listGNmin) != (len(listGeneName))):
        errorFile = open("GeneConversionError.txt", "w")
        errorFile.write("The following genes don't have corresponding Uniprot files : \n")
        for geneName in listGeneName :
            if dicoGeneID[geneName] not in listGN :
                errorFile.write("   -  {} \n".format(geneName))
        errorFile.close()
        #print(listGNmin)
        
    #print(listAN)
    #print(listGN)
    return listAN, listGN

#listeGeneName = convertID({'gene1':'21837392','gene2':'51564','gene3':'140654'})
#filetoListAN("GNtoUniprotID.txt", listeGeneName,{'gene1':'21837392','gene2':'51564','gene3':'140654'} )

        
     
