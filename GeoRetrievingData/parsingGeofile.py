import re
import os

def geoprofile_long(fichier) :
    """Fonction qui récupère les informations d'un fichier txt GEO et les réorganise dans un fichier complet"""

    f=open(fichier)
    f2=open("GEO_FullReorganisedFile.txt","w")
    
    ligne=f.readline()
    longueur=100000
    while ligne!="" :
        if ligne[0:3]=="GDS" :
            nomGDS=ligne
            ligne=f.readline()
            premier=True
            while ligne[0:3]!="GDS" and ligne!="" :
                info=ligne.split('\t')
                if 'Gene title' in info :
                    longueur=len(info)
                    ind=info.index('Gene title')
                    inds=ind+2
                    list_interet=info[ind:inds+1]
                    info.remove(list_interet[0])
                    info.remove(list_interet[1])
                    info.remove(list_interet[2])
                    info.insert(0,list_interet[0])
                    info.insert(1,list_interet[1])
                    info.insert(2,list_interet[2])
                    info.insert(3,"ID manipulation")
                    for i in range(0,len(info)) :
                        if i==len(info)-1 :
                            f2.write(info[i])
                        else :             
                            f2.write(info[i]+"\t")
                    
                    
                elif len(info)==longueur :
                    list_interet=info[ind:inds+1]
                    info.remove(list_interet[0])
                    info.remove(list_interet[1])
                    info.remove(list_interet[2])
                    info.insert(0,list_interet[0])
                    info.insert(1,list_interet[1].upper())
                    info.insert(2,list_interet[2])
                    info.insert(3," ")
                    
                    for i in range(0,len(info)) :
                        if i==len(info)-1 :
                            f2.write(info[i])
                        else :             
                            f2.write(info[i]+"\t")
                else :
                    if premier==True :
                        f2.write("\t"+"\t"+"\t")
                        f2.write(nomGDS[:-1]+"\t")
                        premier=False
                    else :
                        f2.write("\t"+"\t"+"\t"+"\t")
                    for i in range(0,len(info)) :

                        if i==len(info)-1 :
                            f2.write(info[i])
                        else :             
                            f2.write(info[i]+"\t")
                ligne=f.readline()
        else :
            ligne=f.readline()
    f2.close()         
    f.close()



def geoprofile_long_geneSelection(fichier,listgene):
    """Fonction qui récupère les informations d'une liste de gènes donnée et les réorganise dans un fichier complet"""
    
    f=open(fichier)
    f2=open("GEO_TotalReorganisedNotCleaned.txt","w")
    
    ligne=f.readlines()
    print(listgene)
    i=0
    while i<len(ligne) :
        liste=ligne[i].split("\t")
        for j in listgene :
            if j in liste[1] :
                print(j)
                k=i
                while "Gene title" not in ligne[k] :
                    k=k-1
                f2.write(ligne[k])
                f2.write(ligne[k+1])
                f2.write(ligne[i])
                f2.write("\n")           
        i=i+1
        
    f.close()
    f2.close()

def clean_GEO_TotalReorganised() :
    f=open("GEO_TotalReorganisedNotCleaned.txt")
    f2=open('GEO_TotalReorganised.txt',"w")
    
    ligne=f.readlines()
    liste_rep=[]
    i=6
    while i <len(ligne) :

        if ligne[i]==ligne[i-4] :
            liste_rep.append(i)
            liste_rep.append(i-1)
            liste_rep.append(i-2)
            liste_rep.append(i-3)
        i=i+4
    #print(liste_rep)
    
    i=0
    while i<len(ligne) :
        if i not in liste_rep :
            f2.write(ligne[i])
        i=i+1

    f.close()
    f2.close()



#geoprofile_long("leucemie.txt")


#clean_GEO_TotalReorganised()

#listgene=["Sae1","Ranbp2","PML"]
#geoprofile_long("../profile_data(1).txt")
#geoprofile_long_spe('GEO_FullReorganisedFile.txt',listgene)



def geoprofileTrans(fichier) :
    """Fonction qui récupère les informations d'un fichier txt GEO et les réorganise dans un fichier court facilement itérable"""
    f=open(fichier)
    f2=open('TransitionalGeoFile.txt',"w")
    ligne=f.readline()
    
    while ligne!="" :
        if ligne[0:3]=="GDS" :
            nomGDS=ligne
            ligne=f.readline()
            premier=True
            while ligne[0:3]!="GDS" and ligne!="" :
                info=ligne.split('\t')
                if 'Gene title' in info :
                    longueur=len(info)
                    ind=info.index('Gene title')
                    inds=ind+2
                    list_interet=info[ind:inds+1]
                    f2.write("ID manipulation"+"\t")
                    for i in range(0,len(list_interet)) :
                        if i==len(info)-1 :
                            f2.write(list_interet[i])
                        else :             
                            f2.write(list_interet[i]+"\t")
                    f2.write("\n")
                    
                elif len(info)==longueur :
                    list_interet=info[ind:inds+1]
                    f2.write(nomGDS[:-1]+"\t")
                    #print(list_interet)
                    for i in range(0,len(list_interet)) :
                        if i==1 :
                            f2.write(list_interet[i].upper()+"\t")
                        elif i==len(list_interet)-1 :
                            f2.write(list_interet[i])
                        else :             
                            f2.write(list_interet[i]+"\t")
                    f2.write("\n")

                ligne=f.readline()
            f2.write("\n")
        else :
            ligne=f.readline()
            

    f2.close()
    f.close()

    if os.path.getsize("TransitionalGeoFile.txt") == 0 :
        return False
    return True

#"GEO_Trans_GeneSelection.txt"
        
#geoprofileTrans2("GEO_SUMO_Brain_UpDown_profile_data_210920.txt")
#print("a")



def geoprofileTrans_geneSelection(fichier,listgene):
    """Fonction qui récupère les informations d'un fichier txt GEO relatives à des gènes spécifiques
    et les réorganise dans un fichier court facilement itérable"""
    
    f=open(fichier)
    f2=open("GEO_Trans_GeneSelection.txt","w")
    
    ligne=f.readlines()
 
    i=0
    while i<len(ligne) :
        for j in listgene :
            if j in ligne[i] :
                k=i
                while "Gene title" not in ligne[k] :
                    k=k-1
                f2.write(ligne[k])
                f2.write(ligne[i])
                f2.write("\n")           
        i=i+1
        
    f.close()
    f2.close()
    

#geoprofileTrans("../profile_data(1).txt")
#geoprofile_court_spe('GEO_ShortReorganisedFile.txt',listgene)


def geoprofile_court(fichier) :
    """Fonction qui récupère les données GEO d'un fichier déjà réorganisés et n'en ressort que
    les essentielles dans un fichier condensé"""
    f=open(fichier)
    ligne=f.readlines()
    premier_ligne=ligne[0]
    
    #Enlever "ID manip ID gene...."
    i=1
    while i < len(ligne) :
        if ligne[i][0:2]=="ID" :
            del ligne[i]
        i=i+1
    #Liste de liste des informations  
    i=1
    liste=[1]*len(ligne)
    while i<len(ligne) :
        sous_liste=list(re.split('\t',ligne[i]))
        liste[i]=sous_liste
        i=i+1
    liste=liste[1:]

    i=0
    while i<len(liste) :
        if len(liste[i])>1 :
            mot=liste[i][2]
            j=i+1
            while j<len(liste) :
                if mot in liste[j] :
                    liste[i][0]=liste[i][0]+";"+liste[j][0]
                    del liste[j]
                j=j+1
        i=i+1

    

    f2=open('GEO_MinimalReorganisedTrans.txt',"w")
    f2.write(premier_ligne)

    i=0
    print(liste)
    while i <len(liste) :
        if len(liste[i])==1 :
            print(liste[i])
            del liste[i]
        i=i+1
    i=0

    while i<len(liste):
        j=0
        while j<len(liste[i]):
            f2.write(liste[i][j])
            if j!=len(liste[i])-1:
                f2.write("\t")
            j=j+1
        i=i+1
        

    f2.close()
    f.close()


def clean_GEO_MinimalReorganised():
    f=open('GEO_MinimalReorganisedTrans.txt')
    f2=open('GEO_MinimalReorganised.txt',"w")
    ligne=f.readlines()
    i=0
    while i<len(ligne) :
        if ligne[i]!='\n' :
            f2.write(ligne[i])
        i=i+1
    f2.close()
    f.close()
    
    
#clean_GEO_MinimalReorganised()
#geoprofileTrans("GEO_SUMO_Brain_UpDown_profile_data_281220.txt")
#geoprofileTrans_geneSelection("TransitionalGeoFile.txt",)
#geoprofile_court("GEO_Trans_GeneSelection.txt")
  

def recupIDgene(fichier) :
 
    f=open(fichier)
    ligne=f.readline()
    d={}
    
    while ligne!="" :
        
        if ligne[0:2]=="ID" :
            ligne=f.readline()
            
            while ligne!='\n' :
                info=ligne.split('\t')
                if "///" in info[2] :
                    ids=re.split('///', info[2])
                    syms=re.split('///', info[3])
                    print(len(ids))
                    for i in range(0,len(ids)) :
                        if i!=(len(ids)-1):
                            d[ids[i]]=syms[i]
                        else :
                            d[ids[i]]=syms[i][:-1]
                    ligne=f.readline()
                else :
                    d[info[2]]=info[3][:-1]
                    ligne=f.readline()
        ligne=f.readline()
    print(d)
    if '' in d :
        d.pop('')
    return d

##geoprofileTrans("profile_data.txt")
##print(recupIDgene("TransitionalGeoFile.txt"))
##geoprofile_long("profile_data.txt")
#geoprofile_long_geneSelection("GEO_FullReorganisedFile.txt",['CCDC88A','BMPR2','MIR155','MIR155HG','CLNK','HSPA1B','XIST','CD53','BHLHE40','PRDX4','SOCS2','PIM1','RRS1','NOP16','CISH','SMIM14','EGR1','SNORD77','SNORD76','SNORD74','GAS5','SNORD44','SNORD47','SNORD80','SNORD79','SNORD81'])
#clean_GEO_TotalReorganised()
