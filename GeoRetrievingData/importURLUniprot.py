## Célie Da Silva & Bastien Lemarchand ##
## 10/2020

#Importation de modules
import urllib.request
import os


#Fonction   
def URL(AN,GN,path): #AN est l'accession number de la fiche Uniprot
    """Recuperation d'un fichier Uniprot UniprotID.txt sur http://www.uniprot.org, et enregistrement sous forme de fichier texte"""
    try:        #gestion des exceptions
        u=urllib.request.urlopen("https://www.uniprot.org/uniprot/{}.txt".format(AN.upper())) #chemin d'acces à la fiche PDB sur internet 
        lines=u.readlines() #stockages des lignes de la fiche internet dans la variable lines
        u.close()
    except:
        errorFile = open(path+"/UniprotDownloadError.txt", "a")
        errorFile.write("Error: Gene : {} -  impossible to download the file from https://www.uniprot.org/uniprot/{}.txt \n".format(GN.upper(), AN.upper()))
        errorFile.close()
    else:
        print("Accession Number = " + AN)
        file=open(path+ "/PDB_Gene-{}_AN-{}.txt".format(GN,AN),"w") #ouverture du fichier dans dossier utilisateur en mode écriture : chaque fiche est enregistré dans un fichier séparé
        for ligne in lines:
            file.write((ligne.decode("utf8").strip())+"\n") #écriture des lignes stockées dans le fichier correspondant à la fiche 
        file.close()
        return True


