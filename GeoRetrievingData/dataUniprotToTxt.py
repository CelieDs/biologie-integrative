## Célie Da Silva & Bastien Lemarchand ##
## 10/2020

#Importation des modules
import re
import os

def searchDataUniprot(num_access, gene_id, path):
    """Fonction qui permet de parser la fiche Uniprot pour récupérer :
        - ID fiche Uniprot
        - ID du gène
        - Nom du gène
        - Nom de la protéine
        - Localisation(s) Subcellulaire
        - Fonction biologique de la protéine
        - GO Cellular Component, Molecular Function, Biological Process
        - Mutations et Variants
        - Maladies associées à la protéine"""
                
        #Initiation des différentes listes nécéssaires à la récupération des données
    list_IDGN = []  #liste rassemblant les ID des gènes
    list_GN = []    #liste rassemblant les noms des gènes 
    list_RT = []    #liste rassemblant les descriptions de fonction 
    list_GO1 = []   #liste rassemblant un type de termes GO 
    list_GO2 = []   
    list_GO3 = []
    list_MT = []    #liste rassemblant les informations sur la mutagénécité
    list_VARIANT = [] #liste rassemblant les informations sur les variants protéiques
    list_SL = []    #liste rassemblant les localisations cellulaires
    list_DISEASE = []
    dico={'ID': num_access,'ID Gene': '', 'Gene Name' :'' , 'Protein Name':'', 'Subcellular Location(s)':'',
                'Function(s)':'','GOCellularComponent' : '', 'GOMolecularFunction':'', 'GOBiologicalProcess':'',
                'Mutations' : '', 'Variants' : '', 'Disease': ''} #Création d'un dictionnaire pour chaque séquence
    try:
            file = open(path+"/PDB_Gene-{}_AN-{}.txt".format(gene_id, num_access),'r') #Ouverture successive des différents fichiers PDB 
            line=file.readline()                                    #lecture de la premiere ligne

    except:
            print(path+"/PDB_Gene-{}_AN-{}.txt".format(gene_id, num_access))
            print("Le fichier est introuvable")                     #message d'erreur si ouverture/lecture impossible
            
    else:
            while line!="":                                         #tant que la ligne n'est pas vide/tant qu'on estpas a la fin du fichier
                #Recherche de l'identifiant du gène
                        if re.search('^ID', line):
                            line_selected = line.strip()
                            str_to_list = line_selected.split()
                            list_IDGN.append(str_to_list[1])
                            dico['ID Gene'] = list_IDGN[0]
                            
                ##Recherche Nom du gène = GN 
                        elif re.search('^GN', line):                       #si ligne commence par GN 
                                line_selected = line.strip()
                                str_to_list = line_selected.split()     #separation de la ligne en liste au niveau des espaces
                                if ";" in str_to_list[1]:
                                     list_GN.append(str_to_list[1][5:-1]) #enlever ; sinon probleme decallage csv
                                else:
                                     list_GN.append(str_to_list[1][5:]) #recuperation uniquement du nom  
                                dico['Gene Name'] = list_GN[0]           #ajout du nom au dictionnaire qui contiendra tous les noms


                ###NOM PROTEINE
                        elif re.search("^DE   RecName: Full=", line):
                                d=line.index("=")       #identification de la position du premier '=' qui correspond au début du nom de la proteine
                                if "{" in line:
                                        i=line.index("{")       #identification de la position du premier '{'   
                                else:
                                        i=line.index(";")       #ou identification de la position du premier ';' qui correspondent selon les fiches à la fin du nom de la proteine
                                dico["Protein Name"]=(line[d+1:i])
                
                ##localisation subcellulaire
                        elif re.search("^CC   -!- SUBCELLULAR LOCATION:", line):  #si ligne qui comprend la localisation cell
                                i=1
                                list_SL=[]
                                listefinale=[]
                                list_SL.append(line[27:].strip())
                                line2=file.readline()
                                while line2[0:9]==("CC       "):
                                      list_SL.append(" ")
                                      list_SL.append(line2[9:].strip())
                                      line2=file.readline()
                                #print(list_SL)
                                loc="".join(list_SL)
                                exp=re.compile ("[.:][^0-9]{3,50}[;{.]") #création d'une expression régulière: commence par ., puis n'importe quel caractère entre 3 et 30 fois (pour avoir au moins un mot, et pas avoir des expressions trop longues qui parfois sautent un {),  puis fin avec un {
                                expre=exp.findall(loc)  #recherche de toutes les expressions et stockage dans liste
                                if expre!=[]:
                                        for e in expre:
                                                if "," in e:
                                                        fin=e.index(",")
                                                        e=e[:fin+1]
                                                        e2=e[fin+1:-1]
                                                        if "," in e2:
                                                                fin=e2.index(",")
                                                                listefinale.append(e2[fin+1:-1])
                                                                listefinale.append(e2[:fin+1])                                                                       
                                                if ";" in e:
                                                        fin=e.index(";")
                                                        listefinale.append(e[fin+1:-1])
                                                        e=e[:fin+1]
                                                listefinale.append(e[1:-1])
                                dico["Subcellular Location(s)"] = listefinale

                ##Recherche des maladies associées à la protéine
                        elif re.search("^CC   -!- DISEASE:", line):   
                                debut = line.index(":")
                                if 'MIM' in line :
                                        fin = line.index("]")
                                        #print(line[debut+1:fin+1].strip())
                                        dico['Disease'] = line[debut+1:fin+1].strip()

                                else :
                                        disease = [line[debut+1:].strip()]
                                        line = file.readline()
                                        end = False
                                        while end == False:
                                            if "MIM" in line :
                                                fin = line.index("]")
                                                disease.append(line[9:fin+1].strip())
                                                list_DISEASE = " ".join(disease)
                                                dico['Disease'] = list_DISEASE
                                                end = True
        
                                            if "{ECO" in line :
                                                fin = line.index("{ECO")
                                                disease.append(line[9:fin].strip())
                                                list_DISEASE = " ".join(disease)
                                                dico['Disease'] = list_DISEASE
                                                end = True
                                            else :
                                                disease.append(line[9:].strip())
                                                line = file.readline()
                                        if "]" in line :
                                            fin = line.index("]")
                                            disease.append(line[9:fin+1].strip())
                                            list_DISEASE = " ".join(disease)
                                            dico['Disease'] = list_DISEASE
                                                
                ##Recherche Fonction

                        elif re.search ("^RP   FUNCTION", line):
                                while line[0:2] != 'RT':
                                        #print(line)
                                        line = file.readline()
                        
                                transitional_list1 = []
                                while line[0:2] == 'RT':
                                        line_selected = line.strip()
                                        
                                        if ";" in line_selected:
                                                fin = line_selected.index(';')
                                                transitional_list1.append(line_selected[5:fin])

                                        else: 
                                                transitional_list1.append(line_selected[2:])
                                        line = file.readline()
                                        
                                transitional_list2 = ' '.join(transitional_list1)
                                #print(transitional_list2)
                                list_RT.append(transitional_list2)
                                #print(list_RT)
                                dico["Function(s)"] = list_RT
                                


                ##Récupération des GO et tri des différents types de GO
                        elif re.search('^DR   GO',line):
                                if line[21]=="C": #identification des GO correspondant à des composant cellulaires
                                        line_selected = line.strip()
                                        str_to_list = line_selected.split(";") #séparation de la ligne en liste au niveau des ";"
                                        GO= "".join(str_to_list[2]) #jonction des termes de la liste voulus
                                        list_GO1.append(GO)     #ajout à la liste correspondante
                                        dico['GOCellularComponent'] = list_GO1 #ajout de la liste à la clé 'GOCellularComponent' du dictionnaire de la fiche

                                elif line[21]=="F": #identification des GO correspondant à des fonctions moleculaires
                                        line_selected = line.strip()
                                        str_to_list = line_selected.split(';')
                                        GO = "".join(str_to_list[2])
                                        list_GO2.append(GO)
                                        dico['GOMolecularFunction'] = list_GO2
                                elif line[21]=="P":     #identification des GO correspondant à des processus biologiques
                                        line_selected = line.strip()
                                        str_to_list = line_selected.split(";")
                                        GO = "".join(str_to_list[2])
                                        list_GO3.append(GO)
                                        dico['GOBiologicalProcess'] = list_GO3

                ##Récupération des informations sur la mutagénécité
                        #Liste info mutagénécité : [Position_mutation, Type de Mutation, Effet Mutation]
                        elif re.search('^FT   MUTAGEN',line):
                                
                                trans_list=[]
                                

                                #Récupération position mutation
                                trans_list = [line[21:].strip()]
                                line = file.readline()

                                #Récupération type de mutation
                                debut_note = line.index("=")
                                fin_note= line.index(":")
                                trans_list.append(line[debut_note+2:fin_note].strip())#+2 pour ne pas avoir le premier guillemet
        

                                #Récupération conséquences mutation
                                debut_info = fin_note +1
                                trans_list1 = [line[debut_info:].strip()]
                                line = file.readline()
                                
                                while "/evidence" not in line :
                                        line = line.strip()
                                        if line[len(line)-2 == ')"']:
                                            line = line[:-2]
                                        trans_list1.append(line[21:])
                                        line = file.readline()
                                joined_list = ' '.join(trans_list1)
                                trans_list.append(joined_list)

                                list_MT.append(trans_list)

                                #print(list_MT)
               
                                dico["Mutations"] = list_MT


##Dans les fiches Uniprot ça peut aussi s'appeler variant 

                        elif re.search('^FT   VARIANT',line):
                                
                                trans_list=[]

                                #Récupération position mutation
                                trans_list = [line[21:].strip()]
                                line = file.readline()

                                #Récupération type de mutation
                                debut_note = line.index("=")
                                if "(" in line : 
                                    fin_note= line.index("(")
                                    trans_list.append(line[debut_note+2:fin_note].strip())#+2 pour ne pas avoir le premier guillemet
                                    #Récupération conséquences mutation
                                    debut_info = fin_note +1
                                    trans_list1 = [line[debut_info:].strip()]
                                    line = file.readline()

                                    while "/evidence" not in line and "/id" not in line: 
                                        line = line.strip()
                                        if line[len(line)-2 == ')"']:
                                            line = line[:-2]
                                            trans_list1.append(line[21:])
                                            line = file.readline()
                   
                                    joined_list = ' '.join(trans_list1)
                                    trans_list.append(joined_list)

                                    list_VARIANT.append(trans_list)

                        
                                    dico["Variants"] = list_VARIANT
                                
                                else :
                                    i = debut_note+1
                                    word = ""
                                    while line[i] != '\n':
                                        word = word + line[i]
                                        i = i+1
                                    line = file.readline()
                                    if "/evidence"  in line or "/id" in line:
                                        trans_list.append(word)
                                        list_VARIANT.append(trans_list)



                        line=file.readline()
            file.close()
            print("SearchDataOK")
            return(dico)
             
#searchDataUniprot("P29590", "5371")
#PDB_Gene-5371_AN-P29590.txt

#initialisation de variables globales
cle_dico=['ID','ID Gene', 'Protein Name','Subcellular Location(s)',
          'Function(s)','GOCellularComponent','GOMolecularFunction', 'GOBiologicalProcess',
          'Mutations', 'Variants', 'Disease']

###Definition des fonctions

def writeFileTxt(dico, path):
    """Fonction qui génère un document texte contenant les informations récupérées de la fiche Uniprot"""
    f = open(path + '/DataUniprot_Gene-{}_ID-{}.txt'.format(dico['Gene Name'], dico['ID']), 'w') #ouverture du fichier txt en mode ecriture
    print("writing file")
    for cle in dico:
        f.write("{} : ".format(cle))
        if dico[cle] == "":
            f.write('Not Mentioned.')
            f.write('\n')
            
        elif 'ID' not in cle and 'Name' not in cle and 'Disease' not in cle :
            if 'Mutations' not in cle and 'Variants' not in cle:
                f.write('\n')
                list_info = dico[cle]
                for k in range(len(list_info)):
                    f.write(' - {} \n'.format(list_info[k]))
            else :
                #Liste info mutagénécité : [Position_mutation, Type de Mutation, Effet Mutation]
                
                if type(dico['Mutations']) == list:
                    f.write('\n')
                    list_info = dico[cle]
                    for k in range(len(list_info)):
                        ss_list= list_info[k]
                        if len(ss_list)==3:
                            f.write('    Position : {} '.format(ss_list[0]))
                            f.write('; {} \n'.format(ss_list[1]))
                            f.write('Infos : {} \n'.format(ss_list[2]))
                            f.write('\n')

                if type(dico['Variants']) == list:
                    f.write('\n')
                    list_info = dico[cle]
                    #print(list_info)
                    #print(len(list_info))
        
                    for k in range(len(list_info)):
                        ss_list= list_info[k]
                        if len(ss_list) == 3 :
                            f.write('    Position : {} '.format(ss_list[0]))
                            f.write('; {} \n'.format(ss_list[1]))
                            f.write('Infos : {} \n'.format(ss_list[2]))
                            f.write('\n')
                        elif len(ss_list) == 2 :
                            f.write('    Position : {} '.format(ss_list[0]))
                            if "->" in ss_list[1] or "Missing" in ss_list[1]:
                                f.write('; {} \n'.format(ss_list[1]))
                            f.write('Infos : None \n')
                            f.write('\n')
                    
        else :
            f.write('{} \n'.format(dico[cle]))
                
        f.write('\n')
    f.close()
    return True
