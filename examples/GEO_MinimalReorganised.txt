ID manipulation	Gene title	Gene symbol	Gene ID	
GDS775	protein inhibitor of activated STAT 1	PIAS1	56469
GDS1793;GDS1920	small ubiquitin-like modifier 3	SUMO3	20610
GDS1917;GDS3436	ribosomal protein S3	RPS3	6188

GDS2608	thymine-DNA glycosylase	TDG	114521

GDS5033	ubiquitin specific peptidase 25	USP25	304150
